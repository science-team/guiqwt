Source: guiqwt
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Picca Frédéric-Emmanuel <picca@debian.org>
Section: science
Priority: optional
Build-Depends: cython3,
               debhelper-compat (= 13),
               dh-python,
               dh-sequence-numpy3,
               dh-sequence-python3,
               python3-all-dev,
               python3-guidata (>= 3.0.1),
               python3-numpy,
               python3-qwt (>= 0.10),
               python3-setuptools
Build-Depends-Indep: dh-sequence-sphinxdoc <!nodoc>,
                     python3-sphinx <!nodoc>
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/science-team/guiqwt
Vcs-Git: https://salsa.debian.org/science-team/guiqwt.git
Homepage: https://github.com/PierreRaybaut/guiqwt
Rules-Requires-Root: no

Package: python3-guiqwt
Architecture: any
Section: python
Depends: ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Recommends: python3-pydicom
Suggests: python-guiqwt-doc,
          spyder
Description: efficient 2D data-plotting library - Python 3
 The guiqwt Python library provides efficient 2D data-plotting
 features (curve/image visualization and related tools) for
 signal/image processing application development and interactive
 computing. It's based on the scientific modules NumPy and SciPy, and
 the PyQwt plotting widgets for PyQt4 graphical user interfaces.
 .
 This is the Python 3 version of the package.

Package: python-guiqwt-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Description: efficient 2D data-plotting library - Documentation
 The guiqwt Python library provides efficient 2D data-plotting
 features (curve/image visualization and related tools) for
 signal/image processing application development and interactive
 computing. It's based on the scientific modules NumPy and SciPy, and
 the PyQwt plotting widgets for PyQt4 graphical user interfaces.
 .
 This is the documentation of the package.
